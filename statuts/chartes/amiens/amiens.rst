
.. index::
   Chartes ; Amiens
   
   
.. _chartes_amiens:
   
===============================
Chartes d'Amiens (1906)
===============================

:download:`Télécharger Chartes d'Amiens 1906 <La_Charte_d_Amiens.doc>`


Le Congrès confédéral d'Amiens 1906 confirme l'article 2, constitutif de la CGT.

**La CGT groupe, en dehors de toute école politique, tous les travailleurs 
conscients de la lutte à mener pour la disparition du salariat et du patronat**.

Le Congrès considère que cette déclaration est une reconnaissance de la lutte 
de classe, qui oppose sur le terrain économique, les travailleurs en révolte 
contre toutes les formes d'exploitation et d'oppression, tant matérielles que 
morales, mises en oeuvre par la classe capitaliste contre la classe ouvrière. 

Le Congrès précise, par les points suivants, cette affirmation théorique: 
dans l'oeuvre revendicatrice quotidienne, le syndicalisme poursuit la 
coordination des efforts ouvriers, l'accroissement du mieux-être des 
travailleurs par la réalisation d'améliorations immédiates, telles que la 
diminution des heures de travail, l'augmentation des salaires, etc. 

Mais cette besogne n'est qu'un côté de l'oeuvre du syndicalisme il prépare 
l'émancipation intégrale, qui ne peut se réaliser que par l'expropriation 
capitaliste il préconise comme moyen d'action la grève générale et il 
considère que le syndicat, aujourd'hui groupement de résistance, sera, dans 
l'avenir, le groupe de production et de répartition, base de réorganisation 
sociale. 

Le Congrès déclare que cette double besogne, quotidienne et d'avenir, découle 
de la situation des salariés qui pèse sur la classe ouvrière et qui fait, à 
tous les travailleurs, quelles que soient leurs opinions ou leurs tendances 
politiques ou philosophiques, un devoir d'appartenir au groupement essentiel 
qu'est le syndicat. 

Comme conséquence, en ce qui concerne les individus, le Congrès affirme 
l'entière liberté pour le syndiqué, de participer, en dehors 
du groupement corporatif, à telles formes de lutte correspondant à sa conception 
philosophique ou politique, se bornant à lui demander, en réciprocité, de ne 
pas introduire dans le syndicat les opinions qu'il professe au dehors. 

En ce qui concerne les organisations, le Congrès déclare qu'afin que le 
syndicalisme atteigne son maximum d'effet, l'action économique doit s'exercer 
directement contre le patronat, les organisations confédérées n'ayant pas, en 
tant que groupements syndicaux, à se préoccuper des partis et des sectes qui, 
en dehors et à côté, peuvent poursuivre en toute liberté la transformation sociale ". 

SIGNATAIRES
===========

Nous donnons le nom tel qu'il est écrit dans le compte rendu 
puis entre crochets le vrai nom). 

.. hlist::
   :columns: 2
   
   - Marie [Marie François, ouvrier typographe de la Seine] 
   - Cousteau [Cousteau M., ouvrier jardinier] 
   - Menard [Ménard Ludovic,  ouvrier ardoisier à Trélazé] 
   - Chazeaud [Chazeaud Jules, chaudronnier, Lyon] 
   - Bruon [Bruon C., bâtiment] 
   - Ferrier [Ferrier Louis, serrurier, Grenoble] 
   - E. David, B. d. T. Grenoble [David Eugène, plâtrier-peintre, Grenoble] 
   - Latapie [Latapie Jean, métallurgie, Paris] 
   - Médard [Médard Jean-Baptiste] 
   - Merrheim [Merrheim Alphonse, métallurgie] 
   - Delesalle [Delesalle Paul, métallurgiste en instruments de précisions, Paris] 
   - Bled [Bled Jules, jardinier, Seine] 
   - Pouget [Pouget Emile] 
   - Tabard E. [Tabard Etienne, cocher-livreur, Paris] 
   - Bousquet A. [Bousquet Amédée, boulanger, Paris] 
   - Monclard [boulanger, Marseille] 
   - Mazau [Mazaud Jacques, cocher de fiacres, Seine] 
   - Braun [Braun Joseph, ouvrier mécanicien] 
   - Garnery [Garnery Auguste, bijoutier, Seine] 
   - Luquet [Luquet Alexandre, coiffeur, Paris] 
   - Dret [Dret Henri, cordonnier, Paris] 
   - Merzet [Merzet Etienne, mineur, Saône-et-Loire] 
   - Lévy [Lévy Albert, employé] 
   - G. Thil [Thil G., lithographe] 
   - Ader [Ader Paul, ouvrier agricole, Aude] 
   - Yvetot [Yvetot Georges, typographe, Seine] 
   - Delzant [Delzant Charles, verrier, Nord] 
   - H. Galantus [Galantus Henri, ferblantier, Paris] 
   - H.Turpin [Turpin H., voiture] 
   - J. Samay, Bourse du Travail de Paris [Samay J.] 
   - Robert [Robert Charles, palissonneur en peaux, Grenoble] 
   - Bornet [Bornet Jules, bûcheron, Cher] 
   - P. Hervier, Bourse du Travail de Bourges 
   - [Hervier Pierre, Bourges] 
   - Dhooghe, Textile de Reims [Dhooghe Charles, tisseur] 
   - Roullier, Bourse du Travail de Brest [Roullier Jules, électricien, Finistère] 
   - Richer, Bourse du Travail du Mans [Richer Narcisse, ouvrier en chaussures] 
   - Laurent L., Bourse du Travail de Cherbourg [Laurent Léon] 
   - Devilar, courtier de Paris [Devilar C.,] 
   - Bastien, Textile d'Amiens 
   - Henriot, Allumettier, [Henriot H.] 
   - L. Morel de Nice [Morel Léon, employé de commerce] 
   - Sauvage [mouleur en métaux] 
   - Gauthier [Gautier Henri, chaudronnier, Saint-Nazaire]. 


Pour en savoir plus 
===================

Le IXe congrès confédéral, de la CGT, s'est réuni, du 8 au 16 octobre 1906, 
dans une école des faubourgs de la capitale picarde. Il rassemblait 350 délégués 
représentant 1 040 organisations. L'appellation de Charte d'Amiens a été 
donnée à un vote du congrès portant sur les rapports du syndicalisme et 
des partis, adopté le 13 octobre 1906, à la suite d'un scrutin sur deux textes 
contradictoires, qui a donné lieu aux débats les plus animés. 

D'autres résolutions ont été votées par le congrès, les principales portant sur 
les relations syndicales internationales, l'action pour les huit heures, 
le travail aux pièces, les lois ouvrières et l'antimilitarisme. 

La Charte se situe dans la continuité des positions de la CGT affirmées dès sa 
naissance elle est aussi étroitement liée au contexte de l'année 1906. 

La CGT, née en 1895, onze ans plus tôt, ne regroupe guère plus de 200 000 adhérents, 
sur environ 6 millions de salariés français ce qui en fait une des 
confédérations nationales aux effectifs les plus réduits d'Europe, recrutant 
essentiellement dans les milieux d'ouvriers qualifiés et dans l'élite ouvrière. 

Jusqu'en 1914, la CGT comprend en dehors du courant syndicaliste révolutionnaire, 
deux autres tendances importantes, les réformistes et les guesdistes, 
représentants français du courant marxiste. Les guesdistes accordent une 
place subalterne à l'action syndicale à laquelle ils refusent toute possibilité 
d'autonomie et sont partisans de la subordination du syndicat au parti. 

L'adoption de la Charte d'Amiens marque leur défaite définitive. Le congrès 
d'Amiens se tient à la fin d'une période de vive tension. Au plan international, 
deux événements majeurs se sont produits, la Révolution russe de 1905 et la 
crise marocaine dans laquelle France et Allemagne cherchant toutes deux de 
nouveaux territoires à coloniser ont menacé d'en venir aux armes elle s'est 
achevée par les accords d'Algésiras. Au plan national, au-delà de la crise du 
bloc des gauches, deux événements principaux pèsent sur les débats, 
l'affirmation du Parti socialiste SFIO et un regain des luttes ouvrières.



   
   
   
