

.. index::
   pair: Analyse  ; LO



.. _analyse_lo:

==============================
Analyse Lutte Ouvriere
==============================



.. seealso::

   - http://www.grand-angle-libertaire.net/lutte-ouvriere-emancipation-et-domination-dans-une-organisation-politique-elements-dauto-analyse-cooperative-et-critique/



"Lutte ouvrière : émancipation et domination dans une organisation 
politique. Eléments d'autoanalyse critique et réflexive",  texte issu 
du séminaire de recherche militante et libertaire ETAPE (Explorations 
Théoriques Anarchistes Pragmatistes pour l'Emancipation), 14 juin 2017, 
http://www.grand-angle-libertaire.net/lutte-ouvriere-emancipation-et-domination-dans-une-organisation-politique-elements-dauto-analyse-cooperative-et-critique/

Ce texte constitue un document exceptionnel d’autoanalyse coopérative 
et critique de pratiques militantes, issu du séminaire ETAPE. 
C’est une démarche fort rare. Il porte sur le cas de l’organisation 
politique Lutte Ouvrière, par quatre anciens membres de ce groupe et 
une sympathisante actuelle. 

Bien qu’il s’agisse d’un groupe trotskiste, cet effort de distanciation 
proprement libertaire intéressera, bien au-delà des cercles trotskistes, 
une variété de rapports au militantisme, c’est-à-dire ceux qui se 
préoccupent des écueils autoritaires et des limites oligarchiques des 
formes organisationnelles existantes, anciennes ou dites "nouvelles", 
se réclamant pourtant de l’émancipation.

