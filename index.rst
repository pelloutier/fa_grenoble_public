

.. index::
   ! Fédération Anarchiste



=================================================
Documentation FA Grenoble publique
=================================================


.. sidebar:: Doc publique Groupe FA Grenoble

    :Date: |today|
    :AuteurE()s: **fagrenoble AT riseup.net**
    :Pour: FA Grenoble
    :statut: en cours
    
    

.. toctree::
   :maxdepth: 3
   
   congres_FA/congres_FA
   theorie/theorie
   statuts/statuts
   communiques/communiques
   
   

